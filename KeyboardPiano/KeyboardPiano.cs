﻿using System;
using PianoSimulation;
using System.Collections.Generic;
namespace KeyboardPiano

{
  class KeyboardPiano
  {
    const string keys = "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ";
    static void Main(string[] args)
    {
      IPiano piano = new Piano();
      Audio audio = new Audio();
      bool exit = false;

      Console.WriteLine("Welcome to piano simulation. What you wanna do?");
      while (!exit)
      {
        Console.WriteLine("1. Strike Single Key");
        Console.WriteLine("2. Strike Multiple Keys");
        Console.WriteLine("3. List Keys");
        Console.WriteLine("4. Exit");
        Console.Write("Type your option: ");
        string choice = Console.ReadLine();
        switch (choice)
        {
          case "1":
            Console.WriteLine("You chose to strike single key");
            StrikeSingleKey(piano, audio);
            break;
          case "2":
            Console.WriteLine("You chose to strike multiple keys");
            StrikeMultipleKeys(piano, audio);
            break;
          case "3":
            Console.WriteLine("You chose to list key");
            PrintKeys(piano);
            break;
          case "4":
            Console.WriteLine("You chose to exit. Goodbye!");
            exit = true;
            break;
          default:
            Console.WriteLine("Input correct choice.");
            break;
        }
        Console.WriteLine();
      }
    }

    private static void StrikeSingleKey(IPiano piano, Audio audio)
    {
      ConsoleKeyInfo keyInput;

      Console.WriteLine("Enter a key to play a sound. Press escape to leave: ");
      do
      {
        keyInput = Console.ReadKey();
        foreach (char key in keys)
        {
          if (keyInput.KeyChar == key)
          {
            piano.StrikeKey(keyInput.KeyChar);
            PlaySound(piano, audio);
            break;
          }
        }
      } while (keyInput.Key != ConsoleKey.Escape);
    }

    private static void PlaySound(IPiano piano, Audio audio)
    {
      const int Duration = 44100 * 3;

      for (int i = 0; i < Duration; i++)
      {
        audio.Play(piano.Play());
      }
    }

    private static void StrikeMultipleKeys(IPiano piano, Audio audio)
    {
      Console.Write("Enter a series of keys, then press enter to play: ");
      String inputs = Console.ReadLine();
      for (int i = 0; i < inputs.Length; i++)
      {
        foreach (char key in keys)
        {
          if (inputs[i] == key)
          {
            piano.StrikeKey(inputs[i]);
            break;
          }
        }
      }
      PlaySound(piano, audio);
    }

    private static void PrintKeys(IPiano piano)
    {
      List<string> keysInfo = piano.GetPianoKeys();

      foreach (string key in keysInfo)
      {
        Console.WriteLine(key);
      }
    }
  }
}