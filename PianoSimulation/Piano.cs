using System;
using System.Collections.Generic;
namespace PianoSimulation
{
  public class Piano : IPiano
  {
    public string Keys { get; }

    private List<IMusicalString> _wires = new List<IMusicalString>();

    public Piano(string keys = "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ", int samplingRate = 44100)
    {
      Keys = keys;
      const int NumberOfKeys = 24;
      const int StuttgartPitch = 440;
      const int TwelveTone = 12;

      for (int i = 0; i < Keys.Length; i++)
      {
        double numerator = i - NumberOfKeys;
        double frequency = Math.Pow(2, numerator / TwelveTone) * StuttgartPitch;
        IMusicalString wire = new PianoWire(frequency, samplingRate);
        _wires.Add(wire);
      }
    }

    public void StrikeKey(char key)
    {
      if(Keys.Contains(key))
      {
        int wireIndex = Keys.IndexOf(key);
        IMusicalString wire = _wires[wireIndex];
        wire.Strike();
      }
    }

    public double Play()
    {
      double harmonicResult = 0;

      for (int i = 0; i < _wires.Count; i++)
      {
        harmonicResult += _wires[i].Sample();
      }
      return harmonicResult;
    }

    public List<string> GetPianoKeys()
    {
      List<string> keysInfo = new List<string>();

      for (int i = 0; i < Keys.Length; i++)
      {
        string info = "Key: " + Keys[i].ToString() + " Frequency: " + _wires[i].NoteFrequency + "hz";
        keysInfo.Add(info);
      }
      return keysInfo;
    }
  }
}