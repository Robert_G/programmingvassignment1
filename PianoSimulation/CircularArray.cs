using System;

namespace PianoSimulation
{
  public class CircularArray : IRingBuffer
  {
    public int Length { get; }

    private double[] _buffer;
    private int _frontBuffer = 0;
    public CircularArray(int length)
    {
      if(length <= 0){
        throw new ArgumentOutOfRangeException("Length less than or equal to 0");
      }
      Length = length;
      _buffer = new double[Length];
    }
    
    public double Shift(double value)
    {
      const int offset = 1;

      if (_frontBuffer >= Length)
      {
        _frontBuffer = 0;
      }
      double popElement = _buffer[_frontBuffer];
      _buffer[_frontBuffer++] = 0;

      int bufferIndex = _frontBuffer + Length - offset;
      _buffer[bufferIndex % Length] = value;
      return popElement;
    }

    public void Fill(double[] array)
    {
      int arrayIndex = 0;

      if (array.Length > _buffer.Length || array.Length < _buffer.Length)
      {
        throw new IndexOutOfRangeException();
      }

      for (int i = _frontBuffer; i < array.Length + _frontBuffer; i++)
      {
        _buffer[i % Length] = array[arrayIndex++];
      }
    }

    public double this[int index]
    {
      get
      {
        if(index < 0)
        {
          throw new IndexOutOfRangeException();
        }
        int bufferIndex = _frontBuffer + index;
        int moduloIndex = bufferIndex % Length;
        
        return _buffer[moduloIndex];
      }
    }
  }
}

