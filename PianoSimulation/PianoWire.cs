using System;
namespace PianoSimulation
{
  public class PianoWire : IMusicalString
  {
    public double NoteFrequency { get; }
    public int NumberOfSamples { get; }

    private IRingBuffer _buffer;

    public PianoWire(double noteFrequency, int numberOfSamples)
    {
      NoteFrequency = noteFrequency;
      NumberOfSamples = numberOfSamples;
      int Frequency = (int)(NumberOfSamples / NoteFrequency);
      if(Frequency <= 0){
        throw new ArgumentOutOfRangeException("Frequency less than or equal to 0");
      }
      _buffer = new CircularArray(Frequency);
    }

    public void Strike()
    {
      const double MinRange = -0.5;
      const double MaxRange = 0.5;
      double[] samples = new double[(int)(NumberOfSamples / NoteFrequency)];
      Random r = new Random();

      for (int i = 0; i < samples.Length; i++)
      {
        double noise = r.NextDouble() * (MaxRange - MinRange) + MinRange;
        samples[i] = noise;
      }
      _buffer.Fill(samples);
    }

    public double Sample(double decay = 0.996)
    {
      if(_buffer.Length > 2)
      {
        double firstSample = _buffer[0];
        double secondSample = _buffer[1];
        double newSample = ((firstSample + secondSample) / 2) * decay;
        return _buffer.Shift(newSample);
      }
      else 
      {
        throw new ArgumentOutOfRangeException();
      }   
    }
  }
}