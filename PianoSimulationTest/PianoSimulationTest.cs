using Microsoft.VisualStudio.TestTools.UnitTesting;
using PianoSimulation;
using System;
namespace PianoSimulationTest
{
  [TestClass]
  public class PianoSimulationTest
  {
    [TestMethod]
    public void CircularArrayIndexTest()
    {
      IRingBuffer arr = new CircularArray(3);
      double[] arr2 = new double[] { 1, 2, 3 };
      arr.Fill(arr2);
      for (int i = 0; i < 6; i++)
      {
        Console.WriteLine(arr[i]);
        if (i == 5)
        {
          Assert.AreEqual(3, arr[i]);
        }
      }
    }

    [TestMethod]
    public void CircularArrayShiftTest()
    {
      IRingBuffer arr = new CircularArray(5);
      double[] arr2 = new double[] {1,2,3,4,5};
      arr.Fill(arr2);
      arr.Shift(15);
      arr.Shift(16);
      arr.Shift(17);
      arr.Shift(18);
      arr.Shift(19);
      arr.Shift(20);

      for (int i = 0; i < 4; i++)
      {
        Console.WriteLine(arr[i]);
        if (i == 4)
        {
          Assert.AreEqual(20, arr[i]);
        }
      }
    }

    [TestMethod]
    [DataTestMethod]
    [DataRow(new Double[]{1,2,3,4,5,6,7,8,9,10,11})]
    [DataRow(new double[]{1,2,3,4,5})]
    [ExpectedException(typeof(IndexOutOfRangeException))]
    public void CircularArrayIncorrectFillTest(double[] incorrectArr)
    {
      IRingBuffer buffer = new CircularArray(30);
      buffer.Fill(incorrectArr);
    }

    [DataTestMethod]
    [DataRow(0)]
    [DataRow(-3)]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CircularArrayIncorrectSizeTest(int size){
      IRingBuffer buffer = new CircularArray(size);
    }

    [DataTestMethod]
    [DataRow(-5)]
    [DataRow(13)]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void PianoWireIncorrectValuesTest(int noteFrequency)
    {
      IMusicalString wire = new PianoWire(noteFrequency, 2);
      wire.Strike();
      wire.Sample();

    }
    [TestMethod]
    public void PianoWireStrikeRangeTest()
    {
      IMusicalString wire = new PianoWire(440, 44100);
      wire.Strike();
      for (int i = 0; i < 44099; i++)
      {
        try
        {
          double sample = wire.Sample();
          if (sample < -0.5 || sample > 0.5)
          {
            Assert.Fail(sample + "Not between -0.5 or 0.5");
          }
        }
        catch (IndexOutOfRangeException)
        {
          Assert.Fail("IndexOutOfBound error gotten at index" + i);
        }

      }
    }
  }
}
